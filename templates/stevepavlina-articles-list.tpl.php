<?php if (count($articles)): ?>
<table>
	<thead>
		<th><?php print t('View Locally'); ?></th>
		<th><?php print t('View on StevePavlina.com'); ?></th>
	</thead>
	<?php foreach ($articles as $article): ?>
	<?php $url = 'stevepavlina/' . date('Y/m', $article->post_date) . '/' . $article->title_alias; ?>
	<tr>
		<td><?php print l($article->title, $url); ?></td>
		<td><?php print l($article->permalink, $article->permalink); ?></td>
	</tr>
	<?php endforeach; ?>
</table>
<?php endif; ?>
