<div class="stevepavlina-post">
	<h2 id="stevepavlina-post-<?php print $article_id; ?>"><a href="<?php print $permalink; ?>" rel="bookmark" title="Permanent Link: <?php print $plain_title; ?>"><?php print $title ?></a></h2>
	<div class="stevepavlina-article-byline"><small><?php print $byline; ?></small></div>
	<div class="stevepavlina-article-text"><?php print $text; ?></div>
</div>
